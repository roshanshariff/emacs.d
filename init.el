;;; init.el --- Initialize emacs configuration  -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code:

;;;; Setup load-path for other init scripts.

(setq load-prefer-newer t)
;; (setq force-load-messages t)
;; (setq debug-on-error t)

(add-to-list 'load-path (expand-file-name "lisp" user-emacs-directory))

;; A big contributor to startup times is garbage collection. We up the gc
;; threshold to temporarily prevent it from running, then reset it later by
;; enabling `gcmh-mode'. Not resetting it will cause stuttering/freezes.
(setq gc-cons-threshold most-positive-fixnum)

;;;; Bootstrap straight.el

(setq straight-check-for-modifications nil)

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

;;;; Bootstrap config

(straight-use-package 'use-package)
(require 'use-package)

(use-package no-littering :straight t)

(use-package auto-compile
  :straight t
  :config
  (auto-compile-on-load-mode)
  (auto-compile-on-save-mode))

(use-package gcmh
  :straight t
  :hook (after-init . gcmh-mode)
  :custom
  (gcmh-idle-delay 5)
  (gcmh-high-cons-threshold (* 16 1024 1024))
  (gcmh-verbose nil))

(use-package general
  :straight t)

;;;; Load configs for specific features and modes

(load "init-straight")
(load "init-ui")
(load "init-window")
(load "init-themes")
(load "init-modeline")
(load "init-auth")
(load "init-tree")
(load "init-exec-path")
(load "init-editor")
(load "init-company")
(load "init-project")
(load "init-history")
(load "init-help")
;; (load "init-ido")
;; (load "init-ivy")
(load "init-completion")
(load "init-embark")
(load "init-consult")
(load "init-flycheck")
(load "init-outline")
(load "init-org")
(load "init-biblio")
(load "init-conf")
(load "init-cpp")
(load "init-haskell")
(load "init-python")
(load "init-pdf")
(load "init-tex")
(load "init-jupyter")
(load "init-julia")
(load "init-webdev")
(load "init-vc")

;;;; Allow access from emacsclient

(when (< emacs-major-version 28)
  ;; See https://debbugs.gnu.org/cgi/bugreport.cgi?bug=47511
  (advice-add
   'delete-file :before-until
   (defun roshan/prevent-delete-socket (filename &rest trash)
     (equal filename internal--daemon-sockname))))

(use-package server
  :if window-system
  :config (unless (server-running-p) (server-start)))

(use-package atomic-chrome
  :straight t
  :if window-system
  :hook (after-init . atomic-chrome-start-server))

(provide 'init)

;;;; Variables configured via the interactive 'customize' interface

(setq custom-file (no-littering-expand-etc-file-name "custom.el"))
(load custom-file 'noerror 'nomessage 'nosuffix)

;;; init.el ends here

;; Local Variables:
;; coding: utf-8
;; no-byte-compile: t
;; End:
