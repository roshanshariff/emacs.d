;;; init-ivy.el --- set up ivy for completion ;; -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code:

(use-package ivy
  :straight t
  :hook (after-init . ivy-mode)
  :custom
  (ivy-count-format "(%d/%d) ")
  (ivy-fixed-height-minibuffer nil)
  (ivy-use-selectable-prompt t) ;; allow selecting prompt instead of candidate
  (ivy-on-del-error-function #'ignore) ;; don't exit on failed backspace
  (ivy-use-virtual-buffers t)
  (ivy-virtual-abbreviate 'abbreviate) ;; abbreviate virtual buffer names
  :general
  ('ivy-mode-map
   "C-c C-r" #'ivy-resume))

(use-package ivy-rich
  :straight t
  :after ivy
  :config

  (dolist (transformer
           '((counsel-describe-variable
              (:columns
               ((counsel-describe-variable-transformer (:width 40))
                (roshan/+ivy-rich-describe-variable-transformer (:width 10))
                (ivy-rich-counsel-variable-docstring (:face font-lock-doc-face))
                )))))
    (apply #'plist-put ivy-rich-display-transformers-list transformer))

  (ivy-rich-mode)

  :preface

  ;; Taken from doom-emacs/modules/completion/ivy/autoload/ivy.el
  (defun roshan/+ivy-rich-describe-variable-transformer (cand)
    "Previews the value of the variable in the minibuffer"
    (let* ((sym (intern cand))
           (val (and (boundp sym) (symbol-value sym)))
           (print-level 3))
      (replace-regexp-in-string
       "[\n\t\^[\^M\^@\^G]" " "
       (cond ((booleanp val)
              (propertize (format "%s" val) 'face
                          (if (null val)
                              'font-lock-comment-face
                            'success)))
             ((symbolp val)
              (propertize (format "'%s" val)
                          'face 'highlight-quoted-symbol))
             ((keymapp val)
              (propertize "<keymap>" 'face 'font-lock-constant-face))
             ((listp val)
              (prin1-to-string val))
             ((stringp val)
              (propertize (format "%S" val) 'face 'font-lock-string-face))
             ((numberp val)
              (propertize (format "%s" val) 'face 'highlight-numbers-number))
             ((format "%s" val)))
       t))))

(use-package amx
  :straight t
  :general ("M-X" #'amx-major-mode-commands))

(use-package counsel
  :straight t
  :hook (after-init . counsel-mode)
  :custom
  (counsel-mode-override-describe-bindings t)
  :general
  ('counsel-mode-map
   [remap insert-char] #'counsel-unicode-char))

(provide 'init-ivy)
;;; init-ivy.el ends here
