;;; init-consult.el --- Initialize consult and related packages  -*- lexical-binding: t; -*-

;; Copyright (C) 2021  Roshan Shariff

;; Author: Roshan Shariff <roshan@acheron.home.roshan.ca>
;; Keywords: local

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(eval-when-compile (require 'use-package))

(use-package consult
  :straight t
  :demand t
  :general
  ("M-X"                                 #'consult-mode-command
   [remap bookmark-jump]                 #'consult-bookmark            ;; C-x r b
   [remap yank-pop]                      #'consult-yank-replace        ;; M-y
   [remap repeat-complex-command]        #'consult-complex-command     ;; C-x M-:
   [remap switch-to-buffer]              #'consult-buffer              ;; C-x b
   [remap switch-to-buffer-other-window] #'consult-buffer-other-window ;; C-x 4 b
   [remap switch-to-buffer-other-frame]  #'consult-buffer-other-frame  ;; C-x 5 b
   [remap goto-line]                     #'consult-goto-line)          ;; M-g g and M-g M-g
  ('minibuffer-local-map
   "<C-tab>" #'consult-history)
  ('help-map ;; C-h
   "a" #'consult-apropos)        ;; orig. apropos-command
  ('goto-map ;; M-g
   "e" #'consult-compile-error
   "o" #'consult-outline         ;; Alternative: consult-org-heading
   "m" #'consult-mark
   "k" #'consult-global-mark
   "i" #'consult-imenu
   "I" #'consult-imenu-multi)
  ('search-map ;; M-s
   "f" #'consult-find
   "F" #'consult-locate
   "g" #'consult-grep
   "G" #'consult-git-grep
   "r" #'consult-ripgrep
   "l" #'consult-line
   "L" #'consult-line-multi
   "m" #'consult-multi-occur
   "k" #'consult-keep-lines
   "u" #'consult-focus-lines
   "e" #'consult-isearch)        ;; Isearch integration
  ('isearch-mode-map
   "M-e"   #'consult-isearch
   "M-s e" #'consult-isearch
   "M-s l" #'consult-line        ;; needed by consult-line to detect isearch
   "M-s L" #'consult-line-multi) ;; needed by consult-line to detect isearch
  :custom
  (consult-narrow-key "<")
  :init
  ;; Replace `completing-read-multiple' with an enhanced version.
  (advice-add #'completing-read-multiple :override #'consult-completing-read-multiple)

  (setq completion-in-region-function
      (lambda (&rest args)
        (apply (if vertico-mode
                   #'consult-completion-in-region
                 #'completion--in-region)
               args))))

(use-package consult-dir
  :straight t
  :custom
  (consult-dir-project-list-function #'consult-dir-projectile-dirs)
  :general
  ("C-x C-d" #'consult-dir)
  ('minibuffer-local-map
   "C-x C-d" #'consult-dir
   "C-x C-j" #'consult-dir-jump-file))

(use-package consult-flycheck
  :straight t
  :general
  ('goto-map
   "f" #'consult-flycheck))

(provide 'init-consult)
;;; init-consult.el ends here
