(use-package web-mode
  :straight t
  :mode "\\.tsx?\\'"
  :custom
  (web-mode-markup-indent-offset 2)
  (web-mode-css-indent-offset 2)
  (web-mode-code-indent-offset 2))

(use-package typescript-mode
  :straight t)

(use-package tide
  :straight t
  :after (typescript-mode web-mode company flycheck)
  :init
  (defun setup-tide-mode ()
    (when (string-match "tsx?" (file-name-extension (buffer-file-name)))
      (tide-setup)
      (tide-hl-identifier-mode)
      (add-hook 'before-save-hook #'tide-format-before-save)))
  :hook ((web-mode . setup-tide-mode)
         (typescript-mode . setup-tide-mode))
  :config
  (flycheck-add-mode 'typescript-tide 'web-mode)
  (flycheck-add-mode 'javascript-tide 'web-mode))

(use-package add-node-modules-path
  :straight t
  :hook tide-mode)

(defconst company-box-icons--tide-alist
  '(("keyword" . Operator)
    ("module" . Module)
    ("class" . Class)
    ("interface" . Interface)
    ("type" . Variable)
    ("enum" . Enum)
    ("var" . Variable)
    ("local var" . Variable)
    ("function" . Function)
    ("local function" . Function)
    ("method" . Method)
    ("getter" . Method)
    ("setter" . Method)
    ("property" . Property)
    ("constructor" . Constructor)
    ("call" . Method)
    ("index" . Variable)
    ("construct" Method)
    ("parameter" . Variable)
    ("type parameter" . TypeParameter)
    ("primitive type" . Text)
    ("label" . Reference)
    ("alias" . Interface)
    ("const" . Constant)
    ("let" . Variable)))

(defun company-box-icons--tide (candidate)
  (-when-let* ((props (get-text-property 0 'completion candidate))
               (kind (plist-get props :kind))
               (entry (assoc kind company-box-icons--tide-alist)))
    (cdr entry)))

(with-eval-after-load 'company-box
  (add-to-list 'company-box-icons-functions 'company-box-icons--tide))

(provide 'init-webdev)
