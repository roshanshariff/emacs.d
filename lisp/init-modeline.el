;;; init-modeline.el --- Initialize mode line        -*- lexical-binding: t; -*-

;; Copyright (C) 2021  Roshan Shariff

;; Author: Roshan Shariff <roshan@acheron.home.roshan.ca>
;; Keywords: local

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(eval-when-compile (require 'use-package))

(use-package blackout
  :disabled ;; use minions instead
  :straight t)

(use-package minions
  :straight t
  :config (minions-mode))

(use-package hide-mode-line
  :straight t)

(use-package eyeliner
  :disabled
  :straight (eyeliner :type git
                      :host github
                      :repo "dustinlacewell/eyeliner")
  :config
  (require 'eyeliner)
  (eyeliner/install))

(use-package spaceline-all-the-icons
  :disabled
  :straight t
  :custom
  (spaceline-all-the-icons-separator-type 'slant)
  :config
  ;; https://github.com/domtronn/spaceline-all-the-icons.el/issues/41#issuecomment-314114603
  (setq spaceline-responsive nil)
  (spaceline-all-the-icons-theme)
  (spaceline-all-the-icons--setup-anzu))

(use-package doom-modeline
  :straight t
  :hook (after-init . doom-modeline-mode)
  :custom
  (doom-modeline-minor-modes t)
  (doom-modeline-buffer-encoding nil)
  :custom-face
  (mode-line ((t (:inherit variable-pitch))))
  (mode-line-inactive ((t (:inherit variable-pitch))))
  (doom-modeline-buffer-minor-mode ((t (:inherit doom-modeline-buffer-major-mode))))
  :init
  (unless after-init-time
    ;; prevent flash of unstyled modeline at startup
    (setq-default mode-line-format nil)))

(provide 'init-modeline)
;;; init-modeline.el ends here
