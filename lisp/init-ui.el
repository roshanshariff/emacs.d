;;; init-ui.el -- Early initialization of UI elements

;;; Commentary:
;;; This files contains customizations to the Emacs UI
;;; that don't depend on any extra packages, and therefore can be done
;;; early in the load process before package management is available.

;;; Code:

(eval-when-compile (require 'use-package))

(setq
 inhibit-startup-screen t
 ;; Don't resize in steps
 window-resize-pixelwise t
 frame-resize-pixelwise t
 ;; Don't resize frame when changing any of the frame properties below
 frame-inhibit-implied-resize t)

(use-package frame
  :no-require t
  :custom
  (window-divider-default-places t)
  (window-divider-default-bottom-width 5)
  (window-divider-default-right-width 5)
  :hook
  (after-init . window-divider-mode)
  :config
  ;; Disable menu bar, tool bar, and vertical scroll bars
  ;; Do this in early-init.el in emacs 27+
  (menu-bar-mode -1)
  (tool-bar-mode -1)
  (modify-all-frames-parameters
   '((menu-bar-lines . 0)
     (tool-bar-lines . 0)
     ;(vertical-scroll-bars . nil)
     (font . "Source Code Pro-10")))
  (set-face-attribute 'default nil :family "Source Code Pro" :height 100)
  (set-face-attribute 'variable-pitch nil :family "Source Sans Pro" :height 1.05))

;; (when (fboundp 'tool-bar-mode) (tool-bar-mode -1))
;; (when (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))
;; (when (and (fboundp 'menu-bar-mode)
;;            ;; turning off menu bar has no effect in Emacs.app
;;            ;; in EmacsMac.app it disables native fullscreen,
;;            (not (memq window-system '(mac ns))))
;;   (menu-bar-mode -1))

(use-package winner
  :config (winner-mode))

(use-package windmove
  :config
  (windmove-default-keybindings) ;; SHIFT-<dir>
  (windmove-delete-default-keybindings) ;; C-x SHIFT-<dir>
  (windmove-swap-states-default-keybindings '(ctrl shift)) ; buffer-move (below) works better with side windows
  :custom
  (windmove-create-window t))

(use-package buffer-move
  :disabled ;; possibly replaced by windswap?
  :straight t
  :general
  ("C-S-<up>" #'buf-move-up
   "C-S-<down>" #'buf-move-down
   "C-S-<left>" #'buf-move-left
   "C-S-<right>" #'buf-move-right))

(use-package windswap
  :disabled
  :straight t
  :config
  (windswap-default-keybindings 'control 'shift))

(use-package mwheel
  :custom
  (mouse-wheel-tilt-scroll t)
  (mouse-wheel-flip-direction t) ;; flip horizontal scrolling
  (mouse-wheel-scroll-amount '(1 ((shift) . 1) ((control) . text-scale))) ;; one line at a time
  (mouse-wheel-progressive-speed nil) ;; don't accelerate scrolling
  :config
  (mouse-wheel-mode))

(use-package mac-win
  :no-require t
  :if (eq window-system 'mac)
  :init (setq mac-command-modifier 'super
              mac-mouse-wheel-mode t
              mac-option-modifier 'meta
              mac-right-command-modifier 'meta
              mac-right-option-modifier 'control)
  :general
  ([swipe-left] #'mac-next-buffer
   [swipe-right] #'mac-previous-buffer
   "s-|" #'mac-toggle-tab-group-overview
   "s-w" #'delete-frame
   "s-t" (lambda () (interactive)
           (mac-handle-new-window-for-tab nil))))

(setq window-system-default-frame-alist
      '((ns . ((ns-transparent-titlebar . t)
               (ns-appearance . dark)))))

(use-package zoom-frm
  :straight t
  :general ([remap text-scale-adjust] #'zoom-in/out))

(use-package mixed-pitch
  :straight t
  :hook (text-mode . mixed-pitch-mode))

(use-package olivetti
  :straight t
  :hook (Info-mode . olivetti-mode)
  :general
  ('olivetti-mode-map
   "<left-margin> <mouse-1>" #'mouse-select-window
   "<right-margin> <mouse-1>" #'mouse-select-window))

;; Flash cursor after movement or window changes.
(use-package beacon
  :straight t
  :hook (after-init . beacon-mode))

;; Temporarily highlight big changes to text
(use-package volatile-highlights
  :straight t
  :hook (after-init . volatile-highlights-mode))

(provide 'init-ui)
;;; init-ui.el ends here
