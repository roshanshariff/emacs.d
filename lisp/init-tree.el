;;; init-tree.el --- Set up treemacs
;;; Commentary:
;;; Code:

(use-package treemacs
  :straight t
  :defer t
  :config
  (setq treemacs-width 25)
  (treemacs-follow-mode t)
  (treemacs-filewatch-mode t)
  (treemacs-fringe-indicator-mode t)
  (when (executable-find "git")
    (treemacs-git-mode (if treemacs-python-executable 'deferred 'simple)))
  (add-to-list 'treemacs-pre-file-insert-predicates #'treemacs-is-file-git-ignored?)
  :general
  ("M-0"      #'treemacs-select-window
  "C-c t 1"   #'treemacs-delete-other-windows
  "C-c t t"   #'treemacs
  "C-c t B"   #'treemacs-bookmark
  "C-c t f"   #'treemacs-find-file
  "C-c t M-t" #'treemacs-find-tag))

(use-package treemacs-projectile
  :after treemacs projectile
  :straight t)

(use-package treemacs-all-the-icons
  :after treemacs
  :straight t
  :config (treemacs-load-theme "all-the-icons"))

(use-package treemacs-icons-dired
  :after treemacs dired
  :straight t
  :config (treemacs-icons-dired-mode))

(use-package treemacs-magit
  :after treemacs magit
  :straight t)

(provide 'init-tree)
;;; init-tree.el ends here
