;;; init-tex.el --- Initialize TeX and LaTeX packages  -*- lexical-binding: t; -*-

;; Copyright (C) 2021  Roshan Shariff

;; Author: Roshan Shariff <roshan@acheron.home.roshan.ca>
;; Keywords: local, tex, bib

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(eval-when-compile (require 'use-package))

(use-package tex
  :straight auctex
  :defer
  :defines TeX-view-program-list TeX-view-program-selection
  :general ('TeX-mode-map "$" #'self-insert-command)
  :init
  (setq TeX-debug-bad-boxes t
        TeX-parse-self t
        TeX-source-correlate-mode t
        TeX-auto-save t
        TeX-electric-math '("$" . "$")
        TeX-raise-frame-function (lambda () (x-focus-frame (selected-frame))))
  (defvar roshan/Skim-exec "/Applications/Skim.app/Contents/SharedSupport/displayline")
  :custom
  (TeX-view-program-list `(("Skim" (,roshan/Skim-exec " -r -b -g %n %o %b") ,roshan/Skim-exec)))
  (TeX-view-program-selection
   '(((output-dvi has-no-display-manager) "dvi2tty")
     ((output-dvi style-pstricks) "dvips and gv")
     (output-pdf "PDF Tools")
     ((output-pdf output-dvi) "Skim")
     ((output-pdf output-dvi) "Evince")
     (output-html "xdg-open")))
  :config
  (put 'TeX-insert-quote 'delete-selection nil)
  (setq-default TeX-master nil))

(use-package reftex
  :hook (LaTeX-mode . turn-on-reftex)
  :init
  (setq reftex-plug-into-AUCTeX t
        reftex-ref-style-default-list '("Cleveref" "Varioref" "Default")))

(use-package latex-extra
  :straight t
  :hook (LaTeX-mode . latex-extra-mode))

(use-package company-auctex
  :straight t
  :hook (TeX-mode . company-auctex-init))

(use-package auctex-latexmk
  :straight t
  :hook (LaTeX-mode . auctex-latexmk-setup)
  :init
  (setq auctex-latexmk-inherit-TeX-PDF-mode t))

(use-package magic-latex-buffer
  :straight t
  :hook LaTeX-mode
  :config (setq ml/symbols (nconc ml/symbols
                                  '(("\\\\dotsc\\>" . "…")
                                    ("\\\\dotsb\\>" . "⋯")))))

;; From https://github.com/hlissner/doom-emacs/blob/develop/modules/lang/latex/config.el
(with-eval-after-load 'smartparens-latex
  (require 'smartparens)
  (let ((modes '(tex-mode plain-tex-mode latex-mode LaTeX-mode)))
    ;; All these excess pairs dramatically slow down typing in latex buffers,
    ;; so we remove them. Let snippets do their job.
    (dolist (open '("\\left(" "\\left[" "\\left\\{" "\\left|"
                    "\\bigl(" "\\biggl(" "\\Bigl(" "\\Biggl(" "\\bigl["
                    "\\biggl[" "\\Bigl[" "\\Biggl[" "\\bigl\\{" "\\biggl\\{"
                    "\\Bigl\\{" "\\Biggl\\{"
                    "\\lfloor" "\\lceil" "\\langle"
                    "\\lVert" "\\lvert" "`"))
      (sp-local-pair modes open nil :actions :rem))))

(provide 'init-tex)
;;; init-tex.el ends here
