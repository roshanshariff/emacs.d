(use-package julia-mode
  :straight t
  :defer t)

(provide 'init-julia)
