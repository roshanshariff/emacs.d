;;; init-completion.el --- Set up the completing-read framework and enhancements  -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code:

(eval-when-compile (require 'use-package))

;; Taken from https://github.com/minad/vertico#configuration
(use-package emacs
  :custom
  (minibuffer-prompt-properties (append '(cursor-intangible t) minibuffer-prompt-properties))
  (enable-recursive-minibuffers t)
  :config
  (minibuffer-depth-indicate-mode))

(use-package vertico
  :straight (:files ("*.el" "extensions/*.el"))
  :init
  (vertico-mode)
  :custom
  (vertico-resize t)
  (vertico-cycle t))

(use-package vertico-mouse
  :straight vertico
  :config (vertico-mouse-mode))

(use-package vertico-repeat
  :straight vertico
  :general ("C-c C-r" #'vertico-repeat))

(use-package vertico-directory
  :straight vertico
  ;; More convenient directory navigation commands
  :general
  ('vertico-map
   "RET" #'vertico-directory-enter
   "DEL" #'vertico-directory-delete-char
   "M-DEL" #'vertico-directory-delete-word)
  ;; Tidy shadowed file names
  :ghook ('rfn-eshadow-update-overlay-hook #'vertico-directory-tidy))

(use-package orderless
  :straight t
  :custom
  (completion-styles '(orderless)))

(use-package marginalia
  :straight t
  :demand t
  :config
  (marginalia-mode)
  :general
  ('minibuffer-local-map
   "M-A" #'marginalia-cycle))

(provide 'init-completion)
;;; init-completion.el ends here
