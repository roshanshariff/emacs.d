;;; init-conf.el --- set up modes for editing configuration files -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(use-package yaml-mode
  :straight t)

(use-package systemd
  :straight t)

(use-package json-mode
  :straight t)

(use-package udev-mode
  :straight t)

(provide 'init-conf)
;;; init-conf.el ends here
