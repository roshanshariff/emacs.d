(use-package haskell-mode
  :straight t
  :defer
  :init
  (setq haskell-font-lock-symbols t)
  :hook (haskell-mode . (turn-on-haskell-indent
                         turn-on-font-lock
                         turn-on-haskell-doc-mode
                         turn-on-haskell-decl-scan)))

(use-package ghc
  :disabled
  :straight t
  :hook (haskell-mode . ghc-init)
  :commands ghc-debug)

(use-package ghci-completion
  :straight t
  :hook (inferior-haskell-mode . turn-on-ghci-completion))

(provide 'init-haskell)
