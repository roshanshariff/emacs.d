(use-package ido
  :custom
  (ido-enable-flex-matching t)
  (ido-use-virtual-buffers t)
  :config
  (ido-mode)
  (ido-everywhere))

(use-package ido-grid
  :disabled
  :straight t
  :commands ido-grid-enable
  :custom
  (ido-grid-start-small t)
  (ido-grid-bind-keys t)
  :config
  (ido-grid-enable))

(use-package ido-completing-read+
  :straight t
  :config (ido-ubiquitous-mode))

(use-package amx
  :straight t
  :demand
  :config (amx-mode)
  :general ("M-X" #'amx-major-mode-commands))

(use-package ido-better-flex
  :straight t
  :config (ido-better-flex/enable))

(provide 'init-ido)
;;; init-ido.el ends here
