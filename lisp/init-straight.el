;;; init-straight.el --- Customize straight.el package options  -*- lexical-binding: t; -*-

;; Copyright (C) 2021  Roshan Shariff

;; Author: Roshan Shariff <roshan@acheron.home.roshan.ca>
;; Keywords: local

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(eval-when-compile (require 'use-package))

(use-package straight
  :custom
  (straight-host-usernames '((github . "roshanshariff")
                             (gitlab . "roshanshariff")))
  :general
  ("C-c s" '(:prefix-map straight-map
                         :prefix-command straight-prefix
                         :prefix-name "straight.el"))
  ('straight-map
   "c" '("check packages" . straight-check-all)
   "b" '("rebuild packages" . straight-rebuild-all)
   "n" '("normalize packages" . straight-normalize-all)
   "f" '("fetch packages" . straight-fetch-all)
   "F" '("pull packages" . straight-pull-all)
   "m" '("merge packages" . straight-merge-all)
   "P" '("push packages" . straight-push-all)
   "v" '("freeze versions" . straight-freeze-versions)
   "RET" '(:prefix-map straight-package-map
                       :prefix-command straight-package-prefix
                       :prefix-name "straight.el package"))
  ('straight-package-map
   "RET" '("use package" . straight-use-package)
   "r" '("get package recipe" . straight-get-recipe)
   "u" '("visit package website" . straight-visit-package-website)
   "c" '("check package" . straight-check-package)
   "b" '("rebuild package" . straight-rebuild-package)
   "n" '("normalize package" . straight-normalize-package)
   "f" '("fetch package" . straight-fetch-package)
   "F" '("pull package" . straight-pull-package)
   "m" '("merge package" . straight-merge-package)
   "P" '("push package" . straight-push-package)
   "d RET" '("dependencies" . straight-dependencies)
   "d d" '("dependents" . straight-dependents)
   "d f" '("fetch package and deps" . straight-fetch-package-and-deps)
   "d F" '("pull package and deps" . straight-pull-package-and-deps)
   "d m" '("merge package and deps" . straight-merge-package-and-deps)))

(use-package embark :defer t
  :config
  (defvar straight-package-map)
  (defvar embark-straight-map (make-composed-keymap straight-package-map embark-general-map))
  (add-to-list 'embark-keymap-alist '(straight . embark-straight-map))
  (add-to-list 'embark-become-keymaps 'embark-straight-map))

(use-package marginalia :defer t
  :config
  (add-to-list 'marginalia-prompt-categories '("recipe\\|package" . straight)))

(provide 'init-straight)
;;; init-straight.el ends here
