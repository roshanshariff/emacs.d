(use-package projectile
  :straight t
  :hook (after-init . projectile-mode)
  :custom
  (projectile-project-search-path '("~/Dropbox/Projects/" "~/Projects/"))
  (consult-project-root-function #'projectile-project-root)
  ;; (projectile-completion-system 'ivy)
  :general
  ('projectile-mode-map
   "C-c p" #'projectile-command-map))

(use-package counsel-projectile
  :disabled
  :straight t
  :after projectile
  :config (counsel-projectile-mode))

(provide 'init-project)
;;; init-project.el ends here
