;;; init-exec-path.el --- Initialize exec-path-from-shell  -*- lexical-binding: t; -*-

;; Copyright (C) 2021  Roshan Shariff

;; Author: Roshan Shariff <roshan@acheron.home.roshan.ca>
;; Keywords: local, unix

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(eval-when-compile (require 'use-package))

(use-package exec-path-from-shell
  :straight t
  :commands
  exec-path-from-shell-initialize
  :custom
  (exec-path-from-shell-arguments nil)
  (exec-path-from-shell-variables
   '("PATH" "MANPATH" "LANG" "LC_CTYPE" "SSH_AUTH_SOCK"
     "SSH_AGENT_PID" "GPG_AGENT_INFO" "BIBINPUTS"))
  :config
  (when (memq window-system '(mac ns))
    (exec-path-from-shell-initialize)))

(provide 'init-exec-path)
;;; init-exec-path.el ends here
