(use-package savehist
  :config (savehist-mode)
  :custom
  (savehist-additional-variables '(search-ring regexp-search-ring))
  (savehist-autosave-interval 60)
  (savehist-ignored-variables '(woman-topic-history)))

(use-package recentf
  :config (recentf-mode)
  :custom
  (recentf-max-saved-items 500)
  (recentf-max-menu-items 15)
  (recentf-auto-cleanup 'never)
  (recentf-exclude `("/tmp/" "/ssh:"
                     ,no-littering-var-directory
                     ,no-littering-etc-directory)))

(use-package saveplace
  :config (save-place-mode 1))

(provide 'init-history)
