;;; init-outline.el --- Initialize outline modes     -*- lexical-binding: t; -*-

;; Copyright (C) 2021  Roshan Shariff

;; Author: Roshan Shariff <roshan@acheron.home.roshan.ca>
;; Keywords: local

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(eval-when-compile (require 'use-package))

;; TODO Replace outshine with built-in outline-minor-mode in emacs 28
;; In the meantime, perhaps use bicycle
(use-package outshine
  :straight t
  :ghook 'emacs-lisp-mode-hook)

(use-package outline-minor-faces
  :straight t
  :after outline
  :ghook ('outline-minor-mode-hook #'outline-minor-faces-add-font-lock-keywords))

(use-package backline
  :straight t
  :after outline
  :config (advice-add 'outline-flag-region :after 'backline-update))

(provide 'init-outline)
;;; init-outline.el ends here
