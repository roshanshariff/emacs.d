;;; init-python.el --- Python development   -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(use-package elpy
  :straight t
  :commands elpy-enable
  :functions elpy-use-ipython
  :init
  (setq elpy-rpc-virtualenv-path (no-littering-expand-var-file-name "elpy/rpc-venv"))
  (when (executable-find "python3")
    (setq elpy-rpc-python-command "python3"))
  (when (executable-find "jupyter")
    (setq python-shell-interpreter "jupyter"
          python-shell-interpreter-args "console --simple-prompt"
          python-shell-prompt-detect-failure-warning nil))
  :config
  (add-to-list 'python-shell-completion-native-disabled-interpreters
               "jupyter"))

(use-package python
  :defer
  :config (elpy-enable))

(use-package poetry
  :straight t)

(provide 'init-python)
;;; init-python.el ends here
