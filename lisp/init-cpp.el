;;; init-cpp.el --- Initialize C/C++ packages        -*- lexical-binding: t; -*-

;; Copyright (C) 2021  Roshan Shariff

;; Author: Roshan Shariff <roshan@acheron.home.roshan.ca>
;; Keywords: local, c

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(eval-when-compile (require 'use-package))

(use-package cc-mode
  :defer t
  :config
  (c-set-offset 'innamespace 0)
  :custom
  (c-basic-offset 2)
  (c-default-style '((java-mode . "java")
                     (awk-mode "awk")
                     (other . "stroustrup"))))

(use-package clang-format
  :straight t
  :after cc-mode
  :custom
  (clang-format-executable (executable-find "clang-format"))
  :general
  ('c-mode-base-map
   "M-q" #'clang-format-region
   "M-Q" #'clang-format-buffer))

(use-package cmake-ide
  :straight t
  :after cc-mode
  :config (cmake-ide-setup))

(use-package cmake-mode
  :straight t
  :defer t)

;; (use-package cpputils-cmake
;;   :straight t
;;   :commands cppcm-reload-all
;;   :init
;;   (add-hook 'c-mode-common-hook
;;             (lambda ()
;;               (if (derived-mode-p 'c-mode 'c++-mode)
;;                   (cppcm-reload-all))))
;;   (setq cppcm-write-flymake-makefile nil))

(provide 'init-cpp)
;;; init-cpp.el ends here
