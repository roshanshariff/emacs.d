;;; init-vc.el --- Initialize version control systems  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(use-package magit
  :straight t
  :general
  ("C-c g" #'magit-file-dispatch)
  :hook
  (magit-mode . hide-mode-line-mode)
  :custom
  (magit-define-global-key-binding t)
  (magit-display-buffer-function 'magit-display-buffer-fullframe-status-v1)
  (magit-bury-buffer-function 'magit-restore-window-configuration))

(straight-register-package '(ghub :fork t))

(use-package forge
  :straight t
  :after magit
  :custom
  (forge-owned-accounts '(("roshanshariff"))))

(use-package magit-delta
  :straight t
  :if (executable-find "delta")
  :hook (magit-mode . magit-delta-mode)
  :custom
  (magit-delta-default-dark-theme "Nord"))

(use-package git-gutter
  :straight t
  :config
  (global-git-gutter-mode))

(use-package git-timemachine
  :straight t
  :commands git-timemachine)

(use-package git-auto-commit-mode
  :straight t)

(provide 'init-vc)
;;; init-vc.el ends here
