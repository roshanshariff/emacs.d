;;; init-editor.el --- Text editing                  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(eval-when-compile (require 'use-package))

(dolist (cmd '(set-goal-column narrow-to-region narrow-to-page dired-find-alternate-file))
  (put cmd 'disabled nil))

(use-package emacs
  :hook ((prog-mode text-mode) . visual-line-mode)
  :custom
  (initial-major-mode #'fundamental-mode)
  (initial-scratch-message nil)
  (initial-buffer-choice nil)
  (indent-tabs-mode nil)
  (echo-keystrokes 0.001)
  (mark-even-if-inactive t)
  (scroll-margin 2)
  (scroll-conservatively 20)
  :general
  ([remap just-one-space] #'cycle-spacing
   "C-c j" #'delete-indentation
   "C-c C-c" #'save-buffers-kill-emacs))

(use-package emacs
  :when (< emacs-major-version 28)
  :general
  ("C-x x g" #'revert-buffer
   "C-x x r" #'rename-buffer
   "C-x x u" #'rename-uniquely
   "C-x x n" #'clone-buffer
   "C-x x i" #'insert-buffer
   "C-x x t" #'toggle-truncate-lines))

(use-package files
  :custom
  (backup-by-copying t)
  (version-control t)
  (vc-make-backup-files t)
  (delete-old-versions t)
  (view-read-only t)
  (auto-save-file-name-transforms `((".*" ,(no-littering-expand-var-file-name "auto-save/") t))))

(use-package simple
  :custom
  (what-cursor-show-names t)
  (shell-command-prompt-show-cwd t)
  (save-interprogram-paste-before-kill t)
  (use-empty-active-region t)
  (set-mark-command-repeat-pop t)
  (delete-selection-mode t)
  (shift-select-mode nil)
  (column-number-mode t)
  (visual-line-fringe-indicators '(left-curly-arrow right-curly-arrow)))

(use-package dashboard
  :straight t
  :config
  (dashboard-setup-startup-hook)
  :custom
  (initial-buffer-choice (lambda () (dashboard-insert-startupify-lists) (get-buffer dashboard-buffer-name))))

(use-package crux
  :straight t
  :general
  ([remap kill-line] #'crux-smart-kill-line
   [remap kill-whole-line] #'crux-kill-whole-line
   [remap open-line] #'crux-smart-open-line
   "C-x 4 t" #'crux-transpose-windows
   "C-c J" #'crux-top-join-line
   "C-c f r" #'crux-rename-file-and-buffer
   "C-c f d" #'crux-delete-file-and-buffer
   "C-c f o" #'crux-open-with
   "C-c f c" #'crux-copy-file-preserve-attributes
   "C-c f i" #'crux-find-user-init-file
   "C-c f s" #'crux-create-scratch-buffer
   "C-c f u" #'crux-view-url))

(use-package adaptive-wrap
  :straight t
  :hook (visual-line-mode . adaptive-wrap-prefix-mode))

(use-package anchored-transpose
  :straight t
  :general ("C-c C-t" #'anchored-transpose))

(use-package fix-word
  :straight t
  :general
  ([remap upcase-word] #'fix-word-upcase
   [remap downcase-word] #'fix-word-downcase
   [remap capitalize-word] #'fix-word-capitalize))

(use-package whole-line-or-region
  :straight t
  :config
  (whole-line-or-region-global-mode))

(use-package fullframe
  :straight t)

(use-package ibuffer
  :disabled ;; superseded by bufler
  :commands ibuffer ibuffer-quit
  :general ([remap list-buffers] #'ibuffer)
  :config (fullframe ibuffer ibuffer-quit))

(use-package bufler
  :straight t
  :general
  (;;"C-x b" #'bufler-switch-buffer ;; Use counsel-switch-buffer for now
   [remap list-buffers] #'bufler)
  :config (fullframe bufler quit-window))

(use-package all-the-icons-ibuffer
  :straight t
  :hook (after-init . all-the-icons-ibuffer-mode))

(use-package flyspell
  :hook ((text-mode . flyspell-mode)
         (prog-mode . flyspell-prog-mode)))

(use-package unfill
  :straight t
  :general ([remap fill-paragraph] #'unfill-toggle))

(use-package fill-column-indicator
  :straight t
  :commands fci-mode)

(use-package whitespace-cleanup-mode
  :straight t
  :config (global-whitespace-cleanup-mode))

(use-package wc-mode
  :straight t
  :commands wc-mode)

;; Show number of matches while searching
(use-package anzu
  :straight t
  :demand t
  :config
  (global-anzu-mode)
  :general
  ([remap query-replace] #'anzu-query-replace
   [remap query-replace-regexp] #'anzu-query-replace-regexp))

(use-package wgrep
  :straight t)

(use-package rg
  :straight t
  :general
  ("M-s R" #'rg-menu))

(use-package undo-tree
  :straight t
  :config (global-undo-tree-mode))

(use-package expand-region
  :straight t
  :after embark
  :general
  ('embark-region-map
   "SPC" #'er/expand-region))

(use-package move-text
  :straight t
  :disabled ;; superseded by move-dup
  :config (move-text-default-bindings))

(use-package move-dup
  :straight t
  :config (global-move-dup-mode))

(use-package hl-line
  :hook (prog-mode . hl-line-mode))

(use-package hl-sentence
  :straight t
  :hook (text-mode . hl-sentence-mode)
  :init (face-spec-set 'hl-sentence '((t :inherit hl-line)))
  :config (require 'hl-line))

(use-package goto-line-preview
  :disabled
  :straight t
  :general
  ([remap goto-line] #'goto-line-preview
   "M-g g" #'goto-line-preview-relative)
  :preface
  ;; See https://github.com/jcs-elpa/goto-line-preview/pull/10#issuecomment-469850347
  (defun roshan/with-display-line-numbers (f &rest args)
    (let ((display-line-numbers t)
          (display-line-numbers-offset
           (if goto-line-preview--relative-p (- (line-number-at-pos)) 0)))
      (apply f args)))
  :config
  (advice-add 'goto-line-preview :around #'roshan/with-display-line-numbers))

(use-package smartscan
  :disabled ;; superseded by highlight-symbol
  :straight t
  :general
  ("M-n" #'smartscan-symbol-go-forward
   "M-p" #'smartscan-symbol-go-backward))

(use-package highlight-symbol
  :straight t
  :hook
  ((prog-mode . highlight-symbol-mode)
   (prog-mode . highlight-symbol-nav-mode))
  :custom
  (highlight-symbol-on-navigation t))

(use-package hippie-exp
  :general ("M-/" #'hippie-expand))

(use-package rainbow-delimiters
  :disabled
  :straight t
  :commands rainbow-delimiters-mode
  :init
  (add-hook 'prog-mode-hook #'rainbow-delimiters-mode)
  (add-hook 'TeX-mode-hook #'rainbow-delimiters-mode))

(use-package smartparens
  :straight t
  :hook
  (after-init . smartparens-global-mode)
  (after-init . show-smartparens-global-mode)
  :custom
  (sp-show-pair-from-inside t)
  :config
  ;; Enable smartparens in minibuffer. See https://github.com/Fuco1/smartparens/issues/33
  (setq sp-ignore-modes-list (delete 'minibuffer-inactive-mode sp-ignore-modes-list))
  (add-hook 'eval-expression-minibuffer-setup-hook (lambda () (sp-update-local-pairs 'emacs-lisp-mode)))
  (require 'smartparens-config))

(defun roshan/new-empty-buffer ()
  "Create a new empty buffer.
New buffer will be named “untitled” or “untitled<2>”, “untitled<3>”, etc.

It returns the buffer (for elisp programing).

URL `http://ergoemacs.org/emacs/emacs_new_empty_buffer.html'
Version 2017-11-01"
  (interactive)
  (let ((buf (generate-new-buffer "untitled")))
    (switch-to-buffer buf)
    (funcall initial-major-mode)
    (setq buffer-offer-save 'always)
    buf))

(use-package emacs
  :general
  ("C-c f n" #'roshan/new-empty-buffer))

(provide 'init-editor)
;;; init-editor.el ends here
