;;; init-themes.el --- Initialize themes             -*- lexical-binding: t; -*-

;; Copyright (C) 2021  Roshan Shariff

;; Author: Roshan Shariff;; -*- lexical-binding: t; -*- <roshan@acheron.home.roshan.ca>
;; Keywords: local

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(eval-when-compile (require 'use-package))

(require 'general)

(use-package solaire-mode
  :straight t
  :config
  (solaire-global-mode +1))

(use-package doom-themes
  :straight t
  :custom
  (doom-one-brighter-modeline t)
  (doom-one-brighter-comments t)
  (doom-one-comment-bg t)
  (doom-themes-treemacs-theme "doom-colors")
  ;; :hook (after-init . doom-themes-treemacs-config)
  :config
  (doom-themes-visual-bell-config)
  (load-theme 'doom-one t))

(use-package heaven-and-hell
  :straight t
  :config
  (setq heaven-and-hell-theme-type 'dark) ;; Omit to use light by default
  (setq heaven-and-hell-themes
        '((light . doom-one-light)
          (dark . doom-one))) ;; Themes can be the list: (dark . (tsdh-dark wombat))
  ;; Optionally, load themes without asking for confirmation.
  (setq heaven-and-hell-load-theme-no-confirm t)
  :general
  ("C-c <f12>" #'heaven-and-hell-load-default-theme
   "<f12>" #'heaven-and-hell-toggle-theme))

(defun toggle-frame-background-mode ()
  (interactive)
  (setq frame-background-mode
        (if (and (boundp 'frame-background-mode)
                 (eq frame-background-mode 'dark))
            'light 'dark))
  (mapc #'frame-set-background-mode (frame-list))
  (mapc #'enable-theme (reverse custom-enabled-themes)))

(general-def "<f9>" #'toggle-frame-background-mode)

(provide 'init-themes)
;;; init-themes.el ends here
