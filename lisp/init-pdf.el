;;; init-pdf.el --- load pdf-tools and related functionality

;;; Code

(use-package pdf-loader
  :straight (pdf-tools :fork t)
  :config
  (pdf-loader-install)
  (add-hook 'pdf-view-mode-hook #'auto-revert-mode)
  (add-hook 'pdf-view-mode-hook #'pdf-view-midnight-minor-mode))

(use-package pdf-history
  :general
  ('pdf-history-minor-mode-map
   "<mouse-8>" #'pdf-history-backward
   "<mouse-9>" #'pdf-history-forward))

(use-package pdf-continuous-scroll-mode
  :straight (:type git :host github :repo "dalanicolai/pdf-continuous-scroll-mode.el")
  ;; :hook
  ;; (pdf-view-mode . pdf-continuous-scroll-mode)
  )

(provide 'init-pdf)
;;; init-pdf.el ends here
