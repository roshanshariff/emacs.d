;;; init-biblio.el --- Bibliography support          -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code:

(use-package bibtex-actions
  :straight t
  :demand t
  :after embark
  :general
  ("C-c b" :keymap 'bibtex-actions-map)
  ('minibuffer-local-map
   "M-b" #'bibtex-actions-insert-preset)
  :custom
  (bibtex-completion-library-path '("~/Documents/Papers/"))
  (bibtex-completion-bibliography '("~/Documents/Bibliography/zotero.bib"))
  ;; (bibtex-actions-bibliography '("~/Documents/Bibliography/zotero.bib"))
  (bibtex-actions-file-parser-function 'bibtex-actions-file-parser-zotero)
  :config
  ;; Make the 'bibtex-actions' bindings and targets available to `embark'.
  (add-to-list 'embark-target-finders 'bibtex-actions-citation-key-at-point)
  (add-to-list 'embark-keymap-alist '(bib-reference . bibtex-actions-map))
  (add-to-list 'embark-keymap-alist '(citation-key . bibtex-actions-buffer-map)))

(use-package oc
  :after (org bibtex-actions)
  :custom
  (org-cite-global-bibliography bibtex-actions-bibliography)
  (org-cite-export-processors '((latex biblatex) (t csl))))

(use-package oc-bibtex-actions
  :after (oc bibtex-actions)
  :init (straight-use-package 'citeproc)
  :custom
  (org-cite-insert-processor 'oc-bibtex-actions)
  (org-cite-follow-processor 'oc-bibtex-actions)
  (org-cite-activate-processor 'oc-basic))

;;; Org-cite processors
(use-package oc-basic :after oc)
(use-package oc-biblatex :after oc)
(use-package oc-csl :after oc)
(use-package oc-natbib :after oc)

(provide 'init-biblio)
;;; init-biblio.el ends here
