(use-package help
  :no-require
  :custom
  (help-window-select t))

(use-package help-mode
  :no-require
  :hook ((help-mode . visual-line-mode)
         (help-mode . hide-mode-line-mode)))

(use-package find-func
  :general
  ('help-map
   "C-l" #'find-library
   "C-f" #'find-function
   "C-k" #'find-function-on-key
   "C-v" #'find-variable))

(use-package helpful
  :straight (:fork t)
  :custom
  (counsel-describe-function-function #'helpful-callable)
  (counsel-describe-variable-function #'helpful-variable)
  (counsel-describe-symbol-function #'helpful-symbol)
  (counsel-descbinds-function #'helpful-function)
  :general
  ([remap describe-function] #'helpful-callable
   [remap describe-variable] #'helpful-variable
   [remap describe-symbol] #'helpful-symbol
   [remap describe-key] #'helpful-key)
  ('help-map ;; C-h
   "." #'helpful-at-point
   "C-." #'helpful-at-point
   "x" #'helpful-command)
  :hook ((helpful-mode . visual-line-mode)
         (helpful-mode . hide-mode-line-mode)))

(use-package apropos
  :no-require
  :custom
  (apropos-do-all t)
  :hook
  (apropos-mode . hide-mode-line-mode)
  :config
  ;; patch apropos buttons to call helpful instead of help
  ;; https://github.com/hlissner/doom-emacs/blob/ce65645fb87ed1b24fb1a46a33f77cf1dcc1c0d5/core/core-editor.el#L462
  (dolist (fun-bt '(apropos-function apropos-macro apropos-command))
    (button-type-put
     fun-bt 'action
     (lambda (button)
       (helpful-callable (button-get button 'apropos-symbol)))))
  (dolist (var-bt '(apropos-variable apropos-user-option))
    (button-type-put
     var-bt 'action
     (lambda (button)
       (helpful-variable (button-get button 'apropos-symbol))))))

(use-package info
  :hook (Info-mode . mixed-pitch-mode)
  :general
  ('help-map
   "i" #'info-display-manual)
  ('Info-mode-map
   "<mouse-8>" #'Info-history-back
   "<mouse-9>" #'Info-history-forward))

(use-package info-colors
  :straight t
  :hook (Info-selection . info-colors-fontify-node))

(use-package which-key
  :straight t
  :custom
  (which-key-show-early-on-C-h t)
  (which-key-compute-remaps t)
  :custom-face
  (which-key-separator-face ((t (:background unspecified))))
  :config
  ;; Free up "C-h C-h" for which-key usage.
  (define-key help-map (kbd "<help>") nil)
  (define-key help-map (kbd "C-h") nil)
  (which-key-mode))

(provide 'init-help)
