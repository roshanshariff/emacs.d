;;; init-window.el --- Customize buffer and window management  -*- lexical-binding: t; -*-

;; Copyright (C) 2021  Roshan Shariff

;; Author: Roshan Shariff <roshan@acheron.home.roshan.ca>
;; Keywords: local

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(eval-when-compile (require 'use-package))

(use-package emacs
  :general
  ("C-c q" #'quit-window
   "C-c k" #'kill-current-buffer
   "C-c C-k" #'kill-buffer-and-window
   "<mouse-8>" #'mode-line-previous-buffer
   "<mouse-9>" #'mode-line-next-buffer)
  :custom
  (window-sides-slots '(1 0 1 1)))

(use-package popper
  :straight t
  :general
  ("C-`"   #'popper-toggle-latest
   "M-`"   #'popper-cycle
   "C-M-`" #'popper-toggle-type)
  :custom
  (popper-reference-buffers
   '("\\*Messages\\*"
     "Output\\*$"
     "\\*Async Shell Command\\*"
     help-mode helpful-mode
     compilation-mode))
  :ghook 'after-init-hook)

(provide 'init-window)
;;; init-window.el ends here
