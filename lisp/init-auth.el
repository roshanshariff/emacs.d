;;; init-auth.el --- Set up authentication and secrets
;;; Commentary:
;;; Code:

(eval-when-compile (require 'use-package))

(use-package auth-source
  :defer t
  :custom
  (auth-source-save-behavior 'ask)
  (auth-sources (pcase system-type
                  ('gnu/linux '(default))
                  ('darwin '(macos-keychain-generic)))))

(use-package auth-source-xoauth2
  :disabled
  :straight t
  :custom
  (auth-source-xoauth2-creds 'auth-source-xoauth2-pass-creds))

;; (let ((auth-source-creation-defaults '((user . "")))
;;       (auth-source-creation-prompts '((user . "Client ID for %h"))))
;;   (auth-source-search :host "accounts.google.com" :type 'secrets :max 1 :create t))

(provide 'init-auth)
;;; init-auth.el ends here
