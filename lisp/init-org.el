;;; init-org.el --- Initialize org mode              -*- lexical-binding: t; -*-

;; Copyright (C) 2021  Roshan Shariff

;; Author: Roshan Shariff <roshan@acheron.home.roshan.ca>
;; Keywords: local

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(eval-when-compile (require 'use-package))

;; Stolen from https://github.com/hlissner/doom-emacs/blob/develop/modules/lang/org/config.el
(defun +org-init-protocol-lazy-loader-h ()
  "Brings lazy-loaded support for org-protocol, so external programs (like
browsers) can invoke specialized behavior from Emacs. Normally you'd simply
require `org-protocol' and use it, but the package loads all of org for no
compelling reason, so..."
  (defadvice! +org--server-visit-files-a (args)
    "Advise `server-visit-flist' to invoke `org-protocol' lazily."
    :filter-args #'server-visit-files
    (cl-destructuring-bind (files proc &optional nowait) args
      (catch 'greedy
        (let ((flist (reverse files)))
          (dolist (var flist)
            (when (string-match-p ":/+" (car var))
              (require 'server)
              (require 'org-protocol)
              ;; `\' to `/' on windows
              (let ((fname (org-protocol-check-filename-for-protocol
                            (expand-file-name (car var))
                            (member var flist)
                            proc)))
                (cond ((eq fname t) ; greedy? We need the t return value.
                       (setq files nil)
                       (throw 'greedy t))
                      ((stringp fname) ; probably filename
                       (setcar var fname))
                      ((setq files (delq var files)))))))))
      (list files proc nowait)))

  ;; Disable built-in, clumsy advice
  (after! org-protocol
          (ad-disable-advice 'server-visit-files 'before 'org-protocol-detect-protocol-server)))

;;;; Org Mode

(use-package org
  :straight t
  :general
  ("C-c l" #'org-store-link
   "C-c a" #'org-agenda
   "C-c c" #'org-capture)
  :custom
  (org-directory (expand-file-name "~/Dropbox/Notes/"))
  (org-extend-today-until 6)
  (org-link-frame-setup '((file . find-file)))
  (org-pretty-entities t)
  (org-startup-folded nil) ;; Show everything except blocks and drawers
  (org-startup-indented t) ;; `org-indent' indents sub-headings
  (org-startup-with-latex-preview t)
  (org-preview-latex-image-directory (no-littering-expand-var-file-name "org/preview-latex/"))
  (org-preview-latex-default-process 'dvisvgm) ; Needs latex and dvisvgm
  (org-enforce-todo-dependencies t) ;; Make it possible to dim or hide blocked tasks in the agenda view.
  ;; Make C-a, C-e, and C-k smarter with regard to headline tags.
  (org-special-ctrl-a/e t)
  (org-special-ctrl-k t)
  ;; When you create a sparse tree and `org-indent-mode' is enabled,
  ;; the highlighting destroys the invisibility added by
  ;; `org-indent-mode'. Therefore, don't highlight when creating a
  ;; sparse tree.
  (org-highlight-sparse-tree-matches nil)
  (org-hide-emphasis-markers t)
  (org-highlight-latex-and-related '(native))
  (org-latex-packages-alist '(("" "roshan-macros" t)))
  :custom-face
  (org-document-title ((t (:height 2.0))))
  ;; (org-document-info ((t (:foreground "dark orange"))))
  ;; (org-document-info-keyword ((t (:inherit shadow))))
  (org-meta-line ((t (:inherit shadow :foreground unspecified))))
  :init
  ;; Disable Smartparens in Org-related modes, since the keybindings
  ;; conflict.
  (with-eval-after-load 'smartparens
    (add-to-list 'sp-ignore-modes-list #'org-mode))
  :config
  (add-to-list 'org-modules 'org-tempo 'append)
  (add-hook 'org-mode-hook #'variable-pitch-mode)
  (add-hook 'org-mode-hook (lambda () (move-dup-mode -1)))
  ;; Make windmove work in Org mode:
  (add-hook 'org-shiftup-final-hook 'windmove-up)
  (add-hook 'org-shiftleft-final-hook 'windmove-left)
  (add-hook 'org-shiftdown-final-hook 'windmove-down)
  (add-hook 'org-shiftright-final-hook 'windmove-right)

  ;; Latex previews
  (dolist (options '((:scale 2.5)
                     (:foreground default)
                     (:background "Transparent")))
    (apply #'plist-put org-format-latex-options options))

  (advice-add
   'org-open-file :around
   (defun roshan/subprocess-use-pipe (orig-fun &rest args)
     "Apply `ORIG-FUN' to `ARGS' with `process-connection-type' set to nil.
See https://askubuntu.com/questions/646631/emacs-doesnot-work-with-xdg-open"
     (let ((process-connection-type nil))
       (apply orig-fun args)))))

;;;;; Org Mode Customizations

(use-package org-fragtog
  :straight t
  :hook (org-mode . org-fragtog-mode))

(use-package org-superstar ; "prettier" bullets
  :straight t
  :hook (org-mode . org-superstar-mode)
  :init
  :config
  ;; Make leading stars truly invisible, by rendering them as spaces!
  (setq org-superstar-leading-bullet ?\s
        org-superstar-leading-fallback ?\s
        org-hide-leading-stars nil
        org-superstar-todo-bullet-alist
        '(("TODO" . 9744)
          ("[ ]"  . 9744)
          ("DONE" . 9745)
          ("[X]"  . 9745))))

;;;;; Org Journal

(use-package org-journal
  :straight t
  :general
  ("C-c C-j" #'org-journal-new-entry)
  :custom
  (org-journal-dir (expand-file-name "Journal/" org-directory))
  (org-journal-file-type 'daily)
  (org-journal-file-format "%Y-%m-%d.org")
  (org-journal-date-prefix "#+title: ")
  (org-journal-date-format "%A, %d %B %Y")
  (org-journal-time-prefix "* ")
  (org-journal-time-format "%-I:%M %p\n")
  (org-journal-time-format-post-midnight )
  (org-journal-find-file 'find-file))

;;;; Org Roam

(use-package org-roam
  :straight t
  :after org
  ;; :hook
  ;; ((org-roam-find-file . +org-roam-open-buffer-maybe))
  :init
  (setq org-roam-v2-ack t)
  :general
  ("C-c r" #'org-roam-node-find
   "C-c R" #'org-roam-buffer-toggle)
  :custom
  (org-roam-directory org-directory)
  ;; (org-roam-tag-sources '(prop first-directory))
  ;; (org-roam-verbose nil)
  ;; (org-roam-buffer-window-parameters '((no-delete-other-windows . t)))
  :config
  (org-roam-db-autosync-enable)
  (add-hook 'org-roam-find-file-hook #'roshan/org-roam-file-setup 'last)
  ;; (add-hook 'org-roam-buffer-prepare-hook #'roshan/org-roam-buffer-setup)

  :preface

  (defun roshan/org-roam-file-setup ()
     (setq-local org-attach-use-inheritance t)
     (add-to-list
      (make-local-variable 'org-global-properties)
      (cons "DIR" (expand-file-name "Attachments/" org-roam-directory)))
     (setq gac-automatically-push-p t)
     (git-auto-commit-mode)
     (roshan/org-elegant))

  (defun roshan/org-roam-buffer-setup ()
    (hide-mode-line-mode -1)
    (setq-local visual-line-fringe-indicators '(nil nil))
    (visual-line-mode)
    ;; Normally enabled by visual-line-mode-hook, but not needed with
    ;; org-indent-mode:
    (adaptive-wrap-prefix-mode -1)
    (org-indent-mode))

  (defun roshan/org-elegant ()
    (interactive)
    (olivetti-mode)
    (hide-mode-line-mode)
    (hl-sentence-mode -1)
    (beacon-mode -1)
    (setq-local line-spacing 0.2
                org-hidden-keywords '(title)
                visual-line-fringe-indicators '(nil nil))
    (visual-line-mode)
    ;; Normally enabled by visual-line-mode-hook, but not needed with
    ;; org-indent-mode:
    (adaptive-wrap-prefix-mode -1))

  ;; Normally, the org-roam buffer doesn't open until you explicitly
  ;; call `org-roam'. This will open the org-roam buffer when you use
  ;; `org-roam-find-file' (but not `find-file', to limit the scope of
  ;; this behavior).
  (defun +org-roam-open-buffer-maybe ()
    (and (memq 'org-roam-buffer--update-maybe post-command-hook)
         (not (window-parameter nil 'window-side)) ; don't proc for popups
         (not (eq 'visible (org-roam-buffer--visibility)))
         (with-current-buffer (window-buffer)
           (org-roam-buffer--get-create)))))

(use-package deft
  :straight t
  :after org-roam
  :custom
  (deft-recursive t)
  (deft-use-filename-as-title nil)
  (deft-use-filter-string-for-filename t)
  (deft-extensions '("md" "txt" "org"))
  (deft-default-extension "org")
  (deft-directory (expand-file-name "." org-roam-directory)))

(use-package org-roam-protocol)

(use-package org-roam-server
  :straight t
  :disabled
  :custom
  (org-roam-server-host "127.0.0.1")
  (org-roam-server-port 8080)
  (org-roam-server-export-inline-images t)
  (org-roam-server-authenticate nil)
  (org-roam-server-network-poll t)
  (org-roam-server-network-arrows nil)
  (org-roam-server-network-label-truncate t)
  (org-roam-server-network-label-truncate-length 60)
  (org-roam-server-network-label-wrap-length 20))

;;;; Bibliography

(use-package org-ref
  :disabled
  :straight t
  :custom
  (org-ref-default-bibliography (list (expand-file-name "~/Documents/Bibliography/zotero.bib")))
  (org-ref-notes-directory (expand-file-name "Bibliography/" org-roam-directory))
  ;; (org-ref-completion-library 'org-ref-ivy-cite)
  (org-ref-get-pdf-filename-function 'org-ref-get-pdf-filename-helm-bibtex))

(use-package org-roam-bibtex
  :disabled
  :straight t
  :hook (org-roam-mode . org-roam-bibtex-mode)
  :custom
  (bibtex-completion-bibliography (list (expand-file-name "~/Documents/Bibliography/zotero.bib")))
  (bibtex-completion-notes-path (expand-file-name "Bibliography/" org-roam-directory))
  (bibtex-completion-pdf-field "file")
  ;; (bibtex-completion-pdf-open-function 'org-open-file)
  (orb-templates '(("r" "ref" plain #'org-roam-capture--get-point "" :file-name "Bibliography/${citekey}" :head "#+TITLE: ${title}\n#+ROAM_KEY: ${ref}\n\n${author-or-editor}\n\n" :unnarrowed t)))
  :config
  (add-to-list 'orb-preformat-keywords "author-or-editor"))

(use-package ivy-bibtex
  :disabled
  :straight t
  :general ("C-c b" #'ivy-bibtex))

(use-package zotxt
  :straight t
  :disabled
  :hook (org-mode . org-zotxt-mode))

;;;;; pdf-tools integration

(use-package org-pdftools
  :straight t
  :after org
  :config (org-pdftools-setup-link))

(use-package org-noter-pdftools
  :disabled
  :after org-noter
  :config
  (with-eval-after-load 'pdf-annot
    (add-hook 'pdf-annot-activate-handler-functions #'org-noter-pdftools-jump-to-note)))

;;;; LaTeX Justification

;; (defun roshan/justify-org-preview (beg end image &optional imagetype)
;;   (save-match-data
;;     (save-excursion
;;       (goto-char end)
;;       (if (looking-at-p "[[:space:]]*$") ; nothing after end
;;         (let ((line-end (line-end-position))
;;               (line-beg (progn (goto-char beg) (beginning-of-line) (point))))
;;           (looking-at "[[:space:]]*") ; now at beginning of line
;;           (if (eq beg (match-end 0)) ; nothing before beginning
;;               (let* ((imagetype (or (intern imagetype) 'png))
;;                      (image (list 'image :type imagetype :file image :ascent 'center))
;;                      (prop `((space :align-to (- center (0.5 . ,image)))))
;;                      (ov (make-overlay line-beg line-end nil t)))
;;                 (overlay-put ov 'before-string (propertize " " 'display prop))
;;                 (overlay-put ov 'category 'roshan/justify-org-preview))))))))

(defun roshan/justify-org-preview (beg end image &optional imagetype)
  (if (save-excursion
        (goto-char beg)
        (looking-at-p "[[:space:]]*\\\\begin\\|\\\\\\[\\|\\$\\$"))
      (let ((ov (seq-find
                 (lambda (ov) (eq (overlay-get ov 'org-overlay-type) 'org-latex-overlay))
                 (overlays-at beg))))
        (unless (null ov)
          (let* ((ov-img (overlay-get ov 'display))
                 (prop `((space :align-to (- center (0.5 . ,ov-img)))))
                 (left-space (propertize " " 'display prop)))
            (unless (save-match-data
                      (save-excursion
                        (goto-char beg)
                        (beginning-of-line)
                        (looking-at "[[:space:]]*")
                        (<= beg (match-end 0))))
              (setq left-space (concat "\n" left-space)))
            (overlay-put ov 'before-string left-space)
            (unless (save-excursion (goto-char end) (looking-at-p "[[:space:]]*$"))
              (overlay-put ov 'after-string "\n")))))))

(advice-add 'org--make-preview-overlay :after #'roshan/justify-org-preview)

(provide 'init-org)
;;; init-org.el ends here
