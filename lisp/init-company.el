(use-package company
  :straight t
  :hook
  (after-init . global-company-mode)
  :general
  ('company-mode-map
   [remap indent-for-tab-command] #'company-indent-or-complete-common)
  :init
  (setq company-tooltip-align-annotations t
        company-require-match nil))

(use-package company-box
  :straight t
  :hook (company-mode . company-box-mode))

(provide 'init-company)
